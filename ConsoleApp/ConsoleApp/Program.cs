﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            clsObjects cObjects = new clsObjects();
            string jsonString = cObjects.GetJsonString("http://agl-developer-test.azurewebsites.net/people.json");
            cObjects.ParseContent(jsonString);
        }
    }
}
