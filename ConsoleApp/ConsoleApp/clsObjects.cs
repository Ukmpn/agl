﻿using System;
using System.Net;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace ConsoleApp
{
    public class clsObjects
    {
        /// <summary>
        /// Makes a web request to url provided in 
        /// order to process data.
        /// </summary>
        /// <param name="url">location of data source(JSON file)</param>
        public string GetJsonString(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    // log errorText
                }
                throw;
            }

        }

        /// <summary>
        /// Receives the data and prints the output grouped by gender
        /// ordered by the pet names.
        /// </summary>
        /// <param name="JsonString">JSON formatted dataset </param>
        public void ParseContent(string JsonString)
        {
            //string strPeople = @"[{'name':'Bob','gender':'Male','age':23,'pets':[{'name':'Garfield','type':'Cat'},{'name':'Fido','type':'Dog'}]},{'name':'Jennifer','gender':'Female','age':18,'pets':[{'name':'Garfield','type':'Cat'}]},{'name':'Steve','gender':'Male','age':45,'pets':null},{'name':'Fred','gender':'Male','age':40,'pets':[{'name':'Tom','type':'Cat'},{'name':'Max','type':'Cat'},{'name':'Sam','type':'Dog'},{'name':'Jim','type':'Cat'}]},{'name':'Samantha','gender':'Female','age':40,'pets':[{'name':'Tabby','type':'Cat'}]},{'name':'Alice','gender':'Female','age':64,'pets':[{'name':'Simba','type':'Cat'},{'name':'Nemo','type':'Fish'}]}]";

            JArray objPeople = JArray.Parse(JsonString);
            
            var results = from p in objPeople
                          group p["pets"] by p["gender"] into g
                          select new { gender = g.Key, pets = g.ToList() };
            
            foreach(var p in results)
            {
                Console.WriteLine(p.gender.ToString());
                
                foreach(var q in p.pets.Children().OrderBy(x=>x["name"]))
                {
                    Console.WriteLine("\t"+ q["name"].ToString());
                }
            }
            Console.ReadKey();
        }

        
    }

}
